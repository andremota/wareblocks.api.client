﻿using Newtonsoft.Json;

namespace WareBlocks.API.Models
{
    internal class ApiLocation
    {
        [JsonProperty("location")]
        public string LocationUri { get; set; }
    }
}
