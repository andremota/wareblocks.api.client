﻿using Newtonsoft.Json;

namespace WareBlocks.API.Models
{
    public class User
    {
        [JsonProperty("id")]
        public string UserId { get; set; }

        [JsonProperty("permissions")]
        public string[] Permissions { get; set; }
    }
}
