﻿using System;
using System.Net.Http;
using System.Security.Cryptography;
using HawkNet;
using HawkNet.WebApi;

namespace WareBlocks.API
{
    public class WareblocksClientMessageHandler : HawkClientMessageHandler
    {
        public WareblocksClientMessageHandler(HttpMessageHandler innerHandler, HawkCredential credential)
            : base(innerHandler, credential, credential.Id, DateTime.Now, GenerateNonce(), true)
        {
        }

        public WareblocksClientMessageHandler(HawkCredential credential)
            : this(new HttpClientHandler(), credential)
        {
        }

        /// <summary>
        /// Generates a random nonce.
        /// </summary>
        /// <returns>Returns a randomly generated string in Base64.</returns>
        private static string GenerateNonce()
        {

            var buff = new byte[16];

            using (var rnd = RandomNumberGenerator.Create())
            {
                rnd.GetBytes(buff);
            }

            return Convert.ToBase64String(buff);
        }
    }
}
