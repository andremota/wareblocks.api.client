﻿using System;

namespace WareBlocks.API.Exceptions
{
    public class UnknownApiLocationException : Exception
    {
        public UnknownApiLocationException() : base("Could not obtain api location.") { }

        public UnknownApiLocationException(string description) : base(description) { }

        public UnknownApiLocationException(string description, Exception innerException) : base(description, innerException) { }
    }
}
