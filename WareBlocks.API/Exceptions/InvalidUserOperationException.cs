﻿using System;

namespace WareBlocks.API.Exceptions
{
    public class InvalidUserOperationException : Exception
    {
         public InvalidUserOperationException() : base("Could not obtain user information.") { }

        public InvalidUserOperationException(string description) : base(description) { }

        public InvalidUserOperationException(string description, Exception innerException) : base(description, innerException) { }
    }
}
