﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using HawkNet;
using Newtonsoft.Json;
using WareBlocks.API.Exceptions;
using WareBlocks.API.Models;

namespace WareBlocks.API
{
    /// <summary>
    /// Wareblocks api wrapper.
    /// Exports the possible operations over the wareblocks api.
    /// </summary>
    public class WareBlocksApiClient
    {
        private const string HawkAlgorithm = "sha256";

        private volatile string _location;

        public HawkCredential Credentials { get; set; }

        /// <summary>
        /// Initializes the Wareblocks api wrapper with the given credentials.
        /// </summary>
        /// <param name="projectId">Project identifier for the api- can be obtained in the Wareblocks project page.</param>
        /// <param name="projectSecret">Project secret key - can be obtained in the Wareblocks project page.</param>
        public WareBlocksApiClient(string projectId, string projectSecret)
        {
            Credentials = new HawkCredential
            {
                Id = projectId,
                Key = projectSecret,
                Algorithm = HawkAlgorithm
            };
        }

        /// <summary>
        /// Initializes the Wareblocks api wrapper with the given credentials.
        /// </summary>
        /// <param name="projectId">Project identifier for the api- can be obtained in the Wareblocks project page.</param>
        /// <param name="projectSecret">Project secret key - can be obtained in the Wareblocks project page.</param>
        /// <param name="location">Api location</param>
        public WareBlocksApiClient(string projectId, string projectSecret, string location)
            : this(projectId, projectSecret)
        {
            _location = location;
        }

        /// <summary>
        /// Creates an instance of a HttpClient that is used to communicate with the api.
        /// </summary>
        /// <returns>An instance of a HttpClient used to communicate with the api.</returns>
        protected virtual HttpClient CreateHttpClient()
        {
            return new HttpClient(new WareblocksClientMessageHandler(Credentials));
        }

        private readonly object _lock = new object();

        /// <summary>
        /// Get the api location for the given credentials.
        /// </summary>
        public string GetApiLocation()
        {

            if (_location != null)
            {
                return _location;
            }

            lock (_lock)
            {
                // Resolve api location.

                if (_location != null)
                {
                    return _location;
                }

                // Uri of the location method.
                var uriBuilder = new UriBuilder("http://wareblocks.com/api/v1/location");

                var queryString = HttpUtility.ParseQueryString(string.Empty);
                queryString["apiId"] = Credentials.Id;

                uriBuilder.Query = queryString.ToString();

                var response = Task.Run(() => Get(uriBuilder.ToString())).Result;

                if (response.StatusCode != HttpStatusCode.OK)
                    throw new UnknownApiLocationException(
                        string.Format("Could not obtain location for the given api id: {0}. HttpStatus",
                        Credentials.Id, response.StatusCode));

                var apiLocation =  Task.Run(() => Deserialize<ApiLocation>(response)).Result;

                // Obtain the Api uri from the Json response.
                return _location = apiLocation.LocationUri;
            }

        }

        /// <summary>
        /// Obtain the user permissions for a given user id.
        /// </summary>
        /// <param name="userId">The user that will be queried.</param>
        /// <returns>The user permissions for the given user.</returns>
        public async Task<User> GetUserPermissionsAsync(string userId)
        {
            var uriBuilder = new UriBuilder(GetApiLocation())
            {
                Path = string.Format("/project/me/user/{0}/permissions", HttpUtility.UrlEncode(userId))
            };

            var response = await Get(uriBuilder.ToString());

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return await Deserialize<User>(response);
            }

            if (response.StatusCode == HttpStatusCode.NotFound)
            {
                return new User()
                {
                    UserId = userId,
                    Permissions = new string[] { }
                };
            }

            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                throw new InvalidUserOperationException(string.Format("Could not obtain the user {0} information due to invalid credentials. HttpStatus: {1}",
                    userId, response.StatusCode));
            }

            throw new InvalidUserOperationException(string.Format("Could not obtain the user {0} information. HttpStatus: {1}",
                userId, response.StatusCode));

        }

        /// <summary>
        /// For the given user, tries to give the permission if the token is correct.
        /// </summary>
        /// <param name="userId">The user which the permissions will be added to.</param>
        /// <param name="permission">The permissions to add.</param>
        /// <param name="token">The token that gives access to the permission.</param>
        /// <returns>The user with the updated permissions.</returns>
        public async Task<User> GiveUserPermissionsAsync(string userId, string permission, string token)
        {
            var uriBuilder = new UriBuilder(GetApiLocation())
            {
                Path = string.Format("/project/me/user/{0}/permissions", HttpUtility.UrlEncode(userId))
            };

            var toCreate = new
            {
                token,
                permission

            };

            var response = await Create(uriBuilder.ToString(), toCreate);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return await Deserialize<User>(response);
            }

            throw new InvalidUserOperationException(string.Format("Could not give the user {0} the permissions {1}. The token may be invalid. HttpStatus: {2}",
                userId, permission, response.StatusCode));

        }

        /// <summary>
        /// Helper method to deserialize the content from the api response.
        /// </summary>
        /// <typeparam name="T">The type to deserialize to.</typeparam>
        /// <param name="response">The http response obtained from the api.</param>
        /// <returns>An object representing the response.</returns>
        protected static async Task<T> Deserialize<T>(HttpResponseMessage response)
        {
            var cotent = await response.Content.ReadAsStringAsync();
            var obj = await JsonConvert.DeserializeObjectAsync<T>(cotent);
            return obj;
        }

        /// <summary>
        /// Gets an api resource.
        /// </summary>
        /// <param name="path">The absolute path to the resource.</param>
        /// <returns>The api response.</returns>
        protected Task<HttpResponseMessage> Get(string path)
        {
            var requestMessage = new HttpRequestMessage(HttpMethod.Get, path);
            return ApiRequestAsync(requestMessage);
        }


        /// <summary>
        /// Aip method helper to delete a resource.
        /// </summary>
        /// <param name="path">The absolute path to the resource.</param>
        /// <returns></returns>
        protected Task<HttpResponseMessage> Delete(string path)
        {
            var requestMessage = new HttpRequestMessage(HttpMethod.Delete, path);
            return ApiRequestAsync(requestMessage);
        }

        /// <summary>
        /// Api post method helper to create a resource.
        /// </summary>
        /// <param name="path">The absolute path to the http method.</param>
        /// <param name="obj">The objec to be created.</param>
        /// <returns>The api response.</returns>
        protected Task<HttpResponseMessage> Create(string path, object obj)
        {
            var requestMessage = new HttpRequestMessage(HttpMethod.Post, path)
            {
                Content = new StringContent(JsonConvert.SerializeObject(obj))
            };
            requestMessage.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return ApiRequestAsync(requestMessage);
        }

        /// <summary>
        /// Api Put method helper to update a resource.
        /// </summary>
        /// <param name="path">The absolute path to the resource.</param>
        /// <param name="obj">The object to be updated.</param>
        /// <returns>The api response.</returns>
        protected Task<HttpResponseMessage> Update(string path, object obj)
        {
            var requestMessage = new HttpRequestMessage(HttpMethod.Put, path)
            {
                Content = new StringContent(JsonConvert.SerializeObject(obj))
            };
            requestMessage.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return ApiRequestAsync(requestMessage);
        }

        private async Task<HttpResponseMessage> ApiRequestAsync(HttpRequestMessage request)
        {
            using (var client = CreateHttpClient())
            {
                return await client.SendAsync(request);
            }
        }

    }
}
