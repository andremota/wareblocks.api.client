﻿using System.Net.Http;
using HawkNet;
using NUnit.Framework;
using WareBlocks.API.Tests.Helpers;

namespace WareBlocks.API.Tests
{
    #region Mock Class
    public class MockClient : WareBlocksApiClient
    {
        public MockResponseHandler Handler { get; set; }

        public MockClient(string projectId, string projectSecret, MockResponseHandler handler)
            : base(projectId, projectSecret)
        {
            Handler = handler;
        }

        protected override HttpClient CreateHttpClient()
        {
            return new HttpClient(Handler);
        }
    }
    #endregion

    [TestFixture]
    public class WareblocksApiClientTests
    {
        const string LocationMockUri = "http://api.wareblocks.com/";
        const string ProjectId = "fake_id";
        const string ProjectKey = "fake_key";

        public void AddLocationMock(MockResponseHandler mockHandler, string id)
        {
            var locationResult = new { location = LocationMockUri };
            mockHandler.AddMockResponse(string.Format("http://wareblocks.com/api/v1/location?apiId={0}", id), locationResult);
        }

        [Test]
        public async void Wareblock_Api_Client_Should_Be_Able_To_Obtain_The_Correct_Location()
        {
            var mockHandler = new MockResponseHandler();
            AddLocationMock(mockHandler, ProjectId);

            var wareblocksClient = new MockClient(ProjectId, ProjectKey, mockHandler);
            var locationObtained = wareblocksClient.GetApiLocation();

            Assert.AreEqual(LocationMockUri, locationObtained);
        }

        [Test]
        public async void Wareblock_Api_Client_Should_Be_Able_To_Obtain_User_Permissions_For_A_Given_User()
        {
            var mockHandler = new MockResponseHandler();
            AddLocationMock(mockHandler, ProjectId);

            var userResponse = new
            {
                id = "1",
                permissions = new[] { "chat", "some/other/feature" }
            };

            mockHandler.AddMockResponse(string.Format(LocationMockUri + string.Format("project/me/user/{0}/permissions", userResponse.id)),
                userResponse);

            var wareblocksClient = new MockClient(ProjectId, ProjectKey, mockHandler);
            var user = await wareblocksClient.GetUserPermissionsAsync(userResponse.id);

            Assert.AreEqual(userResponse.id, user.UserId);
            Assert.AreEqual(userResponse.permissions, user.Permissions);
        }

        [Test]
        public async void Wareblock_Api_Client_Should_Be_Able_To_Give_User_Permissions_For_A_Given_User()
        {
            var mockHandler = new MockResponseHandler();
            AddLocationMock(mockHandler, ProjectId);

            var userResponse = new
            {
                id = "1",
                permissions = new[] { "chat", "some/other/feature", "new"}
            };

            mockHandler.AddMockResponse(string.Format(LocationMockUri + string.Format("project/me/user/{0}/permissions", userResponse.id)),
                userResponse);

            var wareblocksClient = new MockClient(ProjectId, ProjectKey, mockHandler);
            var user = await wareblocksClient.GiveUserPermissionsAsync(userResponse.id, "new", "asdfghjjkhhgfhgd");

            Assert.AreEqual(userResponse.id, user.UserId);
            Assert.AreEqual(userResponse.permissions, user.Permissions);
        }
    }
}
