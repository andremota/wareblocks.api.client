﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace WareBlocks.API.Tests.Helpers
{
    public class MockResponseHandler : DelegatingHandler
    {
        public Dictionary<Uri, HttpResponseMessage> MockResponse { get; set; }

        public MockResponseHandler(Dictionary<Uri, HttpResponseMessage> mockResponses)
        {
            MockResponse = mockResponses;
            InnerHandler = new HttpClientHandler();
        }

        public MockResponseHandler()
            : this(new Dictionary<Uri, HttpResponseMessage>())
        {
        }


        protected async override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, System.Threading.CancellationToken cancellationToken)
        {
            return MockResponse.ContainsKey(request.RequestUri)
                ? MockResponse[request.RequestUri]
                : new HttpResponseMessage(HttpStatusCode.NotFound) { RequestMessage = request };
        }

        public void AddMockResponse(string uri, object content)
        {
            AddMockResponse(new Uri(uri), content);
        }

        public void AddMockResponse(Uri uri, object content)
        {
            MockResponse.Add(uri,
                BuildMockResponseMessage(content));
        }

        public HttpResponseMessage BuildMockResponseMessage(object content)
        {
            var locationResult = JsonConvert.SerializeObject(content);
            return new HttpResponseMessage()
            {
                Content = new StringContent(locationResult)
            };
        }
    }
}
